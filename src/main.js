import { createApp } from 'vue';
import { createStore } from 'vuex';
import App from './App.vue';

const counterModule = {
    state() {

    },
    mutations: {
        add(state) {
            state.counter++;
        },
    },
    actions: {
        increment(context) {
            setTimeout(function () {
                context.commit('add');
            }, 2000);
        },
    },
    getters: {
        finalCounter(state) {
            return state.counter * 10;
        },
        normalizeCounter(_, getters) {
            const finalCounter = getters.finalCounter;
            if (finalCounter < 0) {
                return 0;
            }
            if (finalCounter > 100) {
                return 100;
            }
            return finalCounter;
        },
    }
};
const store = createStore({
    modules: {
        numbers: counterModule
    },
    state() {
        return {
            counter: 0,
            isLogged: false,
        };
    },
    mutations: {

        setAuth(state, payload) {
            state.isLogged = payload.isAuth;
        }
    },
    getters: {

        userIsAuthencicated(state) {
            return state.isLogged;

        }
    },
    actions: {

        login(context) {
            context.commit('setAuth', { isAuth: true });
        },
        logout(context) {
            context.commit('setAuth', { isAuth: false });
        }
    }
});


const app = createApp(App);

app.use(store);

app.mount('#app');
